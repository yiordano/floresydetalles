<?php
session_start();
                    

class variadas{
    public $estado;
    public $id;
    public $ListadoColores;
    
    public function dibujarcomponente(){
        $estadoactual=$this->estado;
        $codigo=$this->id;
        
        switch($estadoactual){
            case "Si":
                $valor="No";
                break;
            default:
                $valor="Si";
                break;
        }
           $componente="<input type='checkbox' value='$valor.$codigo' name='como[]' class='form-control'>";    
        
        return $componente;
    }
    
    public function horaactual(){
    date_default_timezone_set('America/guatemala');
    return date('Y-m-d-H-i');
}
    
     public function horaactualdisminuida(){
    date_default_timezone_set('America/guatemala');
    return date('Y-m-d');
}

    public function tamanio($tama){
        switch($tama){
            case 1:
                $tamanio="Pequeño";
                break;
            case 2:
                $tamanio="Mediano";
                break;
            case 3:
                $tamanio="Grande";
                break;
                
        }
    return $tamanio;
    }
    
    public  function mes(){
echo "<select name='Mesabuscar' id='' class='form-control'>
     <option value='0'>Seleccione el mes a buscar</option>
     <option value='1'>Enero</option>
     <option value='2'>Febrero</option>
     <option value='3'>Marzo</option>
     <option value='4'>Abril</option>
     <option value='5'>Mayo</option>
     <option value='6'>Junio</option>
     <option value='7'>Julio</option>
     <option value='8'>Agosto</option>
     <option value='9'>Septiembre</option>
     <option value='10'>Octubre</option>
     <option value='11'>Noviembre</option>
     <option value='12'>Diciembre</option>
 </select> ";  
}
    
    public function anio(){
  echo "<select name='anioabuscar' id='' class='form-control'>
     <option value='0'>Seleccione el año a buscar</option>
     <option value='2020'>2020</option>
     <option value='2021'>2021</option>
     
 </select> ";     
 }  
    
    public function generarcontra(){
$abecedario="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnoppqrstuvwxyz";
$numero="0123456789";
$Intnumero=rand(0,strlen($numero)-1);
$IntLetra=rand(0,strlen($abecedario)-1);

	
	
$Strpalabra=$abecedario[$IntLetra].$numero[$Intnumero-1];
while(strlen($Strpalabra)<8){
	$Strpalabra.=$abecedario[rand(0,$IntLetra)].$numero[rand(0,$Intnumero)];
}
		$Strpalabra=str_shuffle($Strpalabra);
        
return $Strpalabra;
    }

    public function colores(){
        $colores=$this->ListadoColores;
        $colores=explode(" ",$colores);
        #print_r($colores);
        #echo count($colores);
        echo "<select class='' name='color'>";
         echo "<option value='Cualquiera esta bien'>Colores disponibles para este producto</option>";  
        foreach($colores as $colores){
          echo "<option value='$colores'>$colores</option>";  
        }
        echo "<select>";
    }
    
    public function edad(){
	
	echo "<select name='edad' id='edad' class='form-control form-control-lg' required> ";
        echo "<option value='0'>Edad</option>";
	for ($intedad=18;$intedad<=66;$intedad++){
	echo "<option value='$intedad'>$intedad</option>";
	}
	echo "</select>";
} 
}

class Conexion_DB{
    
    private $usuario="root";
    private $password="";
    private $db="flores";

    public function conectar(){
       
        try{
           $conexion = new PDO('mysql:host=localhost;dbname='. $this->db, $this->usuario, $this->password);   //<-otra forma de conectar 
            $conexion->exec("set names utf8");
	       $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(Exception $e){
            
        }
     
      return $conexion;  
    }
    
   
    
}

class sitio_web{
    public $estadoavalidar;
    public $codigo;
    public function encabezado(){
        ?>
<header id="header" class="fixed-top header-inner-pages">
    <div class="container-fluid">

        <div class="row justify-content-center">
            <div class="col-xl-9 d-flex align-items-center justify-content-between">
                <h1 class="logo"><a href="index.php">Colocar texto</a></h1>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

                <nav class="nav-menu d-none d-lg-block">
                    <ul>
                        <li class="active"><a href="index.php">Inicio</a></li>
                        <li><a href="#about">Acerca</a></li>
                        <li><a href="#services">Servicios</a></li>
                        <li><a href="#portfolio">Productos</a></li>
 <li><a href="#contact">Contacto</a></li>
                        <li class="drop-down"><a href="">Otras opciones</a>
                            <ul>
                                <li><a href="login.php">Iniciar sesion</a></li>
                                <li><a href="registro.php">Registro</a></li>
                                <li><a href="catalogo.php">Catalogo por mes</a></li>
                               
                                
                                
                                
                                <?php
        if(isset($_SESSION['usuarioadmini'])){
            ?>
            <li class='drop-down'><a href='#'>Opciones administrativas</a>
                                    <ul>
                                        <li><a href='.//index.php#registrar'>Registrar producto</a></li>
                                        <li><a href='adm_productos.php'>dar de baja</a></li>
                                        <li><a href='actualizarproducto.php'>Actualizar Producto</a></li>
                                        <li><a href='adm_registro.php'>Registrar usuario administrativo</a></li>
                                        <li><a href='#'>Contactar a soporte tecnico</a></li>
                                        <li><a href='estadoproductos.php'>Verificar envios</a></li>
                                    </ul>
                                </li>
        <?php
        }
                                ?>
                                

                           
                           
                           
                            </ul>
                        </li>
                       

                    </ul>
                </nav><!-- .nav-menu -->

                <a href="login.php" class="get-started-btn scrollto">empezar</a>
            </div>
        </div>

    </div>
</header><!-- End Header -->

<?php
     } 
    
    public function encabezado_interno(){
           ?>
<header id="header" class="fixed-top header-inner-pages">
    <div class="container-fluid">

        <div class="row justify-content-center">
            <div class="col-xl-9 d-flex align-items-center justify-content-between">
                <h1 class="logo"><a href="../index.php">Colocar texto</a></h1>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

                <nav class="nav-menu d-none d-lg-block">
                    <ul>
                        <li><a href="../index.php">Inicio</a></li>
                        <li><a href="../index.php#about">Acerca</a></li>
                        <li><a href="../index.php#services">Servicios</a></li>
                        <li class="active"><a href="../index.php#portfolio">Productos</a></li>
                              <li><a href="../index.php#contact">Contacto</a></li>
                        <li class="drop-down"><a href="">Otras opciones</a>
                            <ul>
                                <li><a href="#">Iniciar sesion</a></li>
                                                      <?php
        if(isset($_SESSION['usuarioadmini'])){
            ?>
                                <li class="drop-down"><a href="#">Opciones administrativas</a>
                                    <ul>
                                        <li><a href=".//index.php#registrar">Registrar producto</a></li>
                                        <li><a href=".//adm_productos.php">dar de baja</a></li>
                                        <li><a href=".//actualizarproducto.php">Actualizar Producto</a></li>
                                        <li><a href=".//adm_registro.php">Registrar usuario administrativo</a></li>
                                        <li><a href="#">Contactar a soporte tecnico</a></li>
                                         <li><a href='.//estadoproductos.php'>Verificar envios</a></li>
                                    </ul>
                                </li>
                                 <?php
        }
                                ?>
                                <li><a href=".//registro.php">Registro</a></li>
                                <li><a href="../catalogo.php">Catalogo por mes</a></li>
                              
                            </ul>
                        </li>
                      

                    </ul>
                </nav><!-- .nav-menu -->

                <a href="login.php" class="get-started-btn scrollto">Como empezar</a>
            </div>
        </div>

    </div>
</header><!-- End Header -->

<?php  
    }
    
    public function mostrar_productos(){
        ?>
<section id="portfolio" class="portfolio">
    <div class="container">

        <div class="section-title">
            <h2>Productos</h2>
            <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row">
            <div class="col-lg-12 d-flex justify-content-center">
                <ul id="portfolio-flters">
                    <li data-filter="*" class="filter-active">All</li>
                    <li data-filter=".filter-app">Pequeño</li>
                    <li data-filter=".filter-card">Mediano</li>
                    <li data-filter=".filter-web">Grande</li>
                </ul>
            </div>
        </div>

        <div class="row portfolio-container">

            <?php
        
        $producto= new Conexion_DB();
        $Productosamostrar =$producto->conectar()->prepare("SELECT * FROM productos as p, detalle_producto as dp where p.id_productos=dp.id_detalle_producto and darbaja='No' ORDER BY id_detalle_producto desc ");
 
    
            $Productosamostrar->execute();
       
           foreach($Productosamostrar as $Productosamostrar){
               $nombre=$Productosamostrar["nombre"];
               
               $precio=$Productosamostrar["precio"];
               $id=$Productosamostrar["id_productos"];
                $cantidad=$Productosamostrar["cantidad_en_stock"];
               $imagen=$Productosamostrar["imagen1"];
               $tipo=$Productosamostrar["tipoimage1"];
               
               switch($Productosamostrar["tamanio"]){
                  
                   case 1:
                 echo "
                        <div class='col-lg-4 col-md-6 portfolio-item filter-app'>";
                        echo "<img src='data:$tipo;base64," . base64_encode($imagen)."' class='img-fluid' >";
             echo"   <div class='portfolio-info'>
                    <h4>$nombre</h4>
                    <p>Q $precio</p>
                    <p>Cantidad en stock: $cantidad </p>
                  <a href='data:$tipo;base64," . base64_encode($imagen)."' data-gall='portfolioGallery' class='venobox preview-link' title='$nombre'>  <i class='bx bx-plus'></i></a>
                    <a href='portfolio-details.php?Id=$id' class='details-link' title='mas detalles'><i class='bx bx-link'></i></a>
                </div>
            </div>";
                       break;
                       
                   case 2:
                       echo "
                        <div class='col-lg-4 col-md-6 portfolio-item filter-card'>";
                        echo "<img src='data:$tipo;base64," . base64_encode($imagen)."' class='img-fluid'>";
             echo"   <div class='portfolio-info'>
                    <h4>$nombre</h4>
                    <p>Q $precio</p>
                     <p>Cantidad en stock: $cantidad </p>
                    <a href='data:$tipo;base64," . base64_encode($imagen)."' data-gall='portfolioGallery' class='venobox preview-link' title='$nombre'>
                    <i class='bx bx-plus'></i></a>
                    <a href='portfolio-details.php?Id=$id' class='details-link' title='mas detalles'><i class='bx bx-link'></i></a>
                </div>
            </div>";
                       
                       break;
                       
                   case 3:
                       
                  echo "
                        <div class='col-lg-4 col-md-6 portfolio-item filter-web'>";
                        echo "<img src='data:$tipo;base64," . base64_encode($imagen)."' class='img-fluid'>";
             echo"   <div class='portfolio-info'>
                    <h4>$nombre</h4>
                    <p>Q $precio</p>
                     <p>Cantidad en stock: $cantidad </p>
                   <a href='data:$tipo;base64," . base64_encode($imagen)."' data-gall='portfolioGallery' class='venobox preview-link' title='$nombre'> <i class='bx bx-plus'></i></a>
                    <a href='portfolio-details.php?Id=$id' class='details-link' title='mas detalles'><i class='bx bx-link'></i></a>
                </div>
            </div>";
                      
                       break;
               }
           }

            ?>



        </div>

    </div>
</section><!-- End Portfolio Section -->

<?php   
    }
    
    public function preguntas_frecuentes(){
        
        ?>

<section id="faq" class="faq">
    <div class="container-fluid">

        <div class="row">

            <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

                <div class="content">
                    <h3>Preguntas <strong>Frecuentes</strong></h3>
                    <p>
                        Puedes aclarar algunas dudas aca o puedes dejarnos un <a href="comentarios.php" style="color:#009961">comentario</a>
                    </p>
                </div>

                <div class="accordion-list">
                    <ul>
                        <li>
                            <a data-toggle="collapse" class="collapse" href="#accordion-list-1">En cuanto tiempo recibire mi pedido? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                            <div id="accordion-list-1" class="collapse show" data-parent=".accordion-list">
                                <p>
                                    El tiempo puede variar dependiendo de la distancia, trataremos de que llegue lo mas pronto, rogamos tu comprension
                                </p>
                                <p>Hecha por: Leonel sosa</p>
                            </div>
                        </li>

                        <li>
                            <a data-toggle="collapse" href="#accordion-list-2" class="collapsed">El descuento promocional aplica en todo momento? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                            <div id="accordion-list-2" class="collapse" data-parent=".accordion-list">
                                <p>
                                    Lamentablemente no, solo aplica en situaciones especiales.
                                </p>
                            </div>
                        </li>

                        <li>
                            <a data-toggle="collapse" href="#accordion-list-3" class="collapsed">Puede personalizar mi pedido? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                            <div id="accordion-list-3" class="collapse" data-parent=".accordion-list">
                                <p>
                                    Claro puedes personalizar tus pedidos siempre y cuando sea posible para nosotros agradecemos tu preferencia.
                                </p>
                            </div>
                        </li>

                    </ul>
                </div>

            </div>

            <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/faq.jpg");'>&nbsp;</div>
        </div>

    </div>
</section><!-- End Faq Section -->


<?php
    }
    
    public function contacto(){
        ?>

<section id="contact" class="contact">
    <div class="container">

        <div class="section-title">
            <h2>Contactanos</h2>
            <p>Envianos tus comentarios o sugerencias trataremos de responderte lo mas rapido posible.</p>
        </div>
    </div>

    <div>

        <!-- 
        <iframe style="border:0; width: 100%; height: 350px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" allowfullscreen></iframe>
      -->

    </div>

    <div class="container">

        <div class="row mt-5">

            <div class="col-lg-4">
                <div class="info">


                    <div class="address">
                        <i class="ri-map-pin-line"></i>
                        <h4>Ubicacion</h4>
                        <p>A108 Adam Street, New York, NY 535022</p>
                    </div>

                    <div class="email">
                        <i class="ri-mail-line"></i>
                        <h4>Correo Electronico</h4>
                        <p>info@example.com</p>
                    </div>

                    <div class="phone">
                        <i class="ri-phone-line"></i>
                        <h4>Telefono</h4>
                        <p>+1 5589 55488 55s</p>
                    </div>

                </div>

            </div>

            <div class="col-lg-8 mt-5 mt-lg-0">

                <form action="forms/contacto.php" method="post" role="form">
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Tu nombre" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validate"></div>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Tu correo" data-rule="email" data-msg="Please enter a valid email" />
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Titulo" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                        <div class="validate"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                        <div class="validate"></div>
                    </div>

                    <div class="text-center"><button type="submit">Enviar mensaje</button></div>
                </form>

            </div>

        </div>

    </div>
</section><!-- End Contact Section -->


<?php
    }
    
    public function registro_productos(){
?>
<section id="registrar" class="registrar">
    <div class="container">

        <div class="section-title">
            <h2>Registrar Productos</h2>
        </div>
    </div>

    <div>

        <!-- 
        <iframe style="border:0; width: 100%; height: 350px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" allowfullscreen></iframe>
      -->

    </div>

    <div class="container">


        <div class="row mt-5">

            <div class="col-lg-4">
                <div class="info">
                    <div class="address">
                        <h4>Bienvenido</h4>
                        <p><b>
                        <?php
                        $usuario= new funcionesadmin();
                            echo $usuario->buscar_usuario();
                        ?>
                       </b></p>
                        <p>Es un gusto verte de nuevo</p>
                    </div>
                    <br>
                    <div class="phone">
                        <h4>instrucciones para registrar productos:</h4>
                        <p>Ingresa de forma correcta todos los datos al omitir alguna puede fallar</p>
                    </div>

                </div>

            </div>
            <div class="col-lg-8 mt-5 mt-lg-0">


                <form action="forms/procesardatos.php" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                            <input type="text" name="nombreproducto" class="form-control" id="name" placeholder="Nombre del producto" data-rule="minlen:10" maxlength="35" data-msg="Tienes has 35 caracteres" />
                            <div class="validate"></div>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="number" class="form-control" name="precio" id="email" placeholder="Precio del producto" min:50 max="2500" data-msg="El valor minimo es de Q50 y el mas grande es de " 2500 />
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                            <select name="tamanio" id="" class="form-control">
                                <option value="0">Seleccione el tamaño del producto</option>
                                <option value="1">Pequeño</option>
                                <option value="2">Mediano</option>
                                <option value="3">Grande</option>
                            </select>
                            <div class="validate"></div>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="number" class="form-control" name="cantidad" id="email" placeholder="Cantidad de producto a ingresar" min:1 max="150" data-msg="El valor minimo es de 1 y el mas grande es de 150 " />
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="colores" id="subject" placeholder="Colores disponibles  'Recuerda dejar un espacio entre cada color si existe mas de 1'" data-rule="minlen:4" data-msg="Ingrese una cantidad aceptable de caracteres por default solo tiene 4 como minimo" />
                        <div class="validate"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" style="text-align:center" class="form-control" name="horactual" id="subject" readonly="readonly" value=<?php
                                         $estimado=new variadas();
                                         echo $estimado->horaactual(); ?> />
                        <div class="validate"></div>
                    </div>
                    <div class="form-group">
                        <input type="file" name="Archivo1" class="form-control" accept="image/jpeg,image/gif,image/png">
                        <input type="file" name="Archivo2" class="form-control" accept="image/jpeg,image/gif,image/png">
                        <input type="file" name="Archivo3" class="form-control" a="image/jpeg,image/gif,image/png">
                        <div class="validate"></div>
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" name="Descripcion" rows="5" data-rule="required" data-msg="Ingresa la descripcion del producto" placeholder="Descripcion del producto"></textarea>
                        <div class="validate"></div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                            <label for="" class="form-control">Tiene Promocion</label>
                            <input type="checkbox" id="Promocion" name="Promocion" value="Si" class="form-control">

                        </div>
                        <div class="col-md-6 form-group">
                            <label for="" class="form-control">Tiene Descuento</label>
                            <input type="checkbox" id="Descuento" name="Descuento" value="Si" class="form-control">
                        </div>
                    </div>

                    <style>
                        .oculto {
                            display: none;

                        }

                    </style>
                    <div class="form-row">
                        <div class="col-md-6 form-group" class="form-control">
                            <input type="hidden" id="MPromocion" name="MPromocion" class="form-control" readonly>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="hidden" id="CDescuento" name="CDescuento" class="form-control" readonly>
                        </div>
                    </div>

                   

                    <div class="form-group" style="text-align:center">
                        <input type="submit" value="Registrar productos">
                    </div>



                </form>

                <script>
                    let mensaje = "";
                    $('#Promocion').on('click', function() {


                        if ($("#Promocion").is(':checked')) {

                          mensaje = prompt('Ingrese la promocion que desee agregar a este producto');
                                    $('#MPromocion').val(mensaje);
                                } else {
                             mensaje = " ";
                                    $('#MPromocion').val(mensaje);
                        }


                    })

                    $('#Descuento').on('click', function() {
                       
                        
                        if ($("#Descuento").is(':checked')) {

                                let numero=prompt('Ingrese unicamente numeros');
                                numero= parseInt(numero,10);
                        
                             if(numero<0 || isNaN(numero)){
                           numero=0;
                             $('#CDescuento').val(numero);
                           }else if(numero>100){
                               numero=100;
                                $('#CDescuento').val(numero);
                           }else{
                               $('#CDescuento').val(numero);
                           }
                            
                                } else {
                                    numero=''; 
                                      $('#CDescuento').val(numero);
                            
                        }
                        
                       
                        
                      
                    });

                </script>

            </div>

        </div>

    </div>

</section><!-- End Contact Section -->

<?php
  }
    
    public function estadoadministrativo(){
        $estado=$this->estadoavalidar;
        $con=new Conexion_DB();  
        
        switch($estado){
            case 'Si':
                $stm=$con->conectar()->prepare("Select p.nombre as codigonombre,p.id_productos, pc.detalles,pc.color, pc.id_producto_comprados,pc.cantidad_compradas,pc.totalacancelar,pc.totalcondescuento,pc.nombre,pc.telefono,
                pc.direccion
                from productos_comprados as pc, productos as p where entregado='No' and is_valido=:isvalido and pc.id_producto=p.id_productos
                order by fechavalidacion desc ");
                $stm->bindParam (":isvalido", $estado);
        
                break;
            case 'No':
                $stm=$con->conectar()->prepare("Select p.nombre as codigonombre,p.id_productos, pc.detalles,pc.color,   pc.id_producto_comprados,pc.cantidad_compradas,pc.totalacancelar,pc.totalcondescuento,pc.nombre,pc.telefono,
                pc.direccion
                from productos_comprados as pc, productos as p where entregado='No' and is_valido=:isvalido and pc.id_producto=p.id_productos
                 order by fechavalidacion desc");
                $stm->bindParam (":isvalido", $estado);
        
                break;
            default:
                $stm=$con->conectar()->prepare("Select p.nombre as codigonombre,p.id_productos, pc.detalles,pc.color,  pc.id_producto_comprados,pc.cantidad_compradas,pc.totalacancelar,pc.totalcondescuento,pc.nombre,pc.telefono,
                pc.direccion
                from productos_comprados as pc, productos as p where entregado='No' and pc.id_producto=p.id_productos
                 order by fechavalidacion desc");
        
                break;
        }
        $stm->execute();
        
        ?>
<div class="container">


    <h3>Productos por entregar</h3>
    <?php
          
        foreach($stm as $stm){
            echo " <ul >
          <li id='Codigo'><b> Codigo:   </b>{$stm['id_producto_comprados']} </li> 
          <li><b> Nombre del producto:  </b>{$stm['codigonombre']}  </li> 
          <li><b> Cantidad:             </b>{$stm['cantidad_compradas']}  </li> 
          <li><b> Subtotal:             </b>{$stm['totalacancelar']} </li> 
          <li><b> Total:                </b>{$stm['totalcondescuento']} </li> 
          <li><b> Nombre:               </b>{$stm['nombre']}  </li> 
          <li><b> Telefono:             </b>{$stm['telefono']}  </li> 
          <li><b> Direccion:            </b>{$stm['direccion']} </li>
          <li><b> Color:            </b>{$stm['color']} </li>
          <li><b> Otros detalles:            </b>{$stm['detalles']} </li>
           <li style='list-style:none'>
           &nbsp;
          </li>
          <li style='list-style:none'>
         <form method='post'>";
        switch($estado){
            case 'Si':
                $componente="
                  <input type='hidden' value='{$stm['id_producto_comprados']} {$stm['id_productos']} {$stm['cantidad_compradas']}' name='codigogenerado'>  
                  <input type='submit' value='Entregar pedido' id='button'>
                ";
                echo $componente;
                break;
                case 'No':
               $componente="
                  <input type='hidden' value='{$stm['id_producto_comprados']}' name='codigogenerado'>  
                  <input type='submit' value='Cambiar estado' id='button'>
                ";
                echo $componente;
                break;
            default:
               $componente="";
                echo $componente;
                break;
        }
        echo " </form>
          </li>
          </ul>
          <ul style='border-top: 1px solid #eee'>
          </ul>
          <br>
            ";
        }
        ?>


</div>

<?php
    }
    
    public function tituloestado(){
        $estado=$this->estadoavalidar;
        
        switch($estado){
            case 'Si':
                $estado="Pendientes de entrega";
                break;
                case 'No':
                $estado="Pendientes de validar";
                break;
            default:
                $estado="Pendientes de entregar o validar";
                break;
        }
        
        return $estado;
    }
    
    public function procesar(){
         $con=new Conexion_DB(); 
   
        $codigoprocesado=$this->codigo;
         $estado=$this->estadoavalidar;
    
       
        
        switch($estado){
            case 'Si':
                 $codigoprocesado=explode(" ",$codigoprocesado);
                $idProductocomprado=$codigoprocesado[0];
                $idproducto=$codigoprocesado[1];
                $cantidad=$codigoprocesado[2];
                
                  $actualizar=$con->conectar()->prepare("Update productos_comprados set entregado='Si' where id_producto_comprados=:idProductoComprado ");
                    $actualizar->bindParam (":idProductoComprado", $idProductocomprado);
                    $actualizar->execute();
                
                    $actualizo=$actualizar->rowcount();
                
                if($actualizo>0){
                    $seleccionar=$con->conectar()->prepare("SELECT cantidad_en_stock FROM productos where id_productos=$idproducto");
                    $seleccionar->execute();
                    
                    foreach($seleccionar as $producto){
                        
                    }
                    $cantidadproducto=$producto['cantidad_en_stock'];
                    
                    if($cantidad<=$cantidadproducto){
                        $cantidadproducto=$cantidadproducto-$cantidad;
                        
                        
$ingresar=$con->conectar()->prepare("Update productos set cantidad_en_stock=$cantidadproducto where id_productos=$idproducto"); 
                  
                        #echo "Update productos set cantidad_en_stock=$cantidadproducto FROM productos where id_productos=$idproducto";
                        $ingresar->execute();
                        
                    }else{
                        echo "
                        <script>alert('El producto no se puede vender ya que no hay existencias')</script>";
                        $actualizar=$con->conectar()->prepare("Update productos_comprados set entregado='No' where id_producto_comprados=:idProductoComprado ");
                        $actualizar->bindParam (":idProductoComprado", $idProductocomprado);
                        $actualizar->execute();
                    }
                }
                    
                
                break;
            case 'No':
        $actualizar=$con->conectar()->prepare("Update productos_comprados set is_valido='Si' where id_producto_comprados='$codigoprocesado' ");
                    $actualizar->execute();
               
                   $conteo=$actualizar->rowcount();
                
                if($conteo>0){
                  echo "
                        <script>alert('El producto ha sido aprobado con exito ya puedes entregarlo')</script>";  
                }
                break;
            default:
                break;
                
        }
        
        
        
    }
    
    public function comentarios(){
        
    }
    
}

class conteo{
    
    public $mes;
    public $anio;
    public $estado;
    
    public function productomes(){
        $mes=$this->mes;
        $anio=$this->anio;
        $estimado=new Conexion_DB();
                                        
    
         $conectar=$estimado->conectar()->prepare("SELECT count(p.id_productos) 
        FROM productos as p, detalle_producto as dp where dp.mes=:mes and dp.anio=:anio and p.id_productos=dp.id_detalle_producto");
        
        $conectar->bindParam (":mes", $mes);
        $conectar->bindParam (":anio", $anio);
        $conectar->execute();
    
        foreach($conectar as $Result){
            
        }
        
        echo $Result[0];
       
    }  
    
    public function tiene_promocion(){
        $mes=$this->mes;
        $anio=$this->anio;
        $estimado=new Conexion_DB();
                                        
    
         $conectar=$estimado->conectar()->prepare("SELECT count(p.id_productos) 
        FROM productos as p, detalle_producto as dp where dp.mes=:mes and dp.anio=:anio and p.id_productos=dp.id_detalle_producto and have_promocion='Si'");
        
        $conectar->bindParam (":mes", $mes);
        $conectar->bindParam (":anio", $anio);
        $conectar->execute();
    
        foreach($conectar as $Result){
            
        }
        
        echo $Result[0];   
    }
    
    public function estadoproducto(){
        $productoestado=$this->estado;
        
        switch($productoestado){
            case "Si":
                $estado="En baja";
                break;
                
            case "No":
                 $estado="En alta";
                break;
                
            default:
                break;
                
        }
        
         return $estado;
    }
    
    public function cantidaddeusuarios(){
        $con= new Conexion_DB();
        
       $conteo=$con->conectar()->prepare("SELECT count(id_usuario) FROM usuarios");
        $conteo->execute();
       foreach($conteo as $conteo){
           $cantidad=$conteo[0];
       }
        
     
        echo  $cantidad;
    }
}

class compras{
    public $id;
    public $ordenes;
    public $subtotal;
    public $total;
    public $color; 
    public $detalles;
    
    public function componente(){
        $id=$this->id;
        $ordenes=$this->ordenes;
        $subtotal=$this->subtotal;
        $total=$this->total;
        $color=$this->color;
        $detalles=$this->detalles;
        
        if(isset($_SESSION['usuario'])){
            $disponible="readonly";
            $usuario=$_SESSION['usuario'][0];
             $con=new Conexion_DB();  
            
            $buscarusuario=$con->conectar()->prepare('SELECT nombre,correo,direccion,telefono from usuarios where id_usuario=:idusuario');
            $buscarusuario->bindParam (":idusuario", $usuario);
            $buscarusuario->execute();
            
            foreach($buscarusuario as $buscarusuario){}
            
        }else{
            $disponible='';
            $buscarusuario['nombre']='';
            $buscarusuario['correo']='';
            $buscarusuario['direccion']='';
            $buscarusuario['telefono']='';
        }
            
       ?>


<form action="" method="post">
    <div class="form-row">
        <div class="col-md-6 form-group">
            <input type="text" placeholder="Ingresa tu nombre" name="Nombre" value="<?php echo $buscarusuario['nombre']  ?>" class="form-control" required <?php echo $disponible;?>>
        </div>
        <div class="col-md-6 form-group">
            <input type="email" placeholder="Ingresa un correo electronico valido" value="<?php echo $buscarusuario['correo']  ?>" name="Correo" class="form-control" required <?php echo $disponible;?>>
            <div class="validate"></div>
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-6 form-group">
            <input type="text" placeholder="Ingresa la direccion de envio" value="<?php echo $buscarusuario['direccion']  ?>" name="Direccion" class="form-control" required <?php echo $disponible;?>>
            <div class="validate"></div>
        </div>
        <div class="col-md-6 form-group">
            <input type="number" placeholder="Ingrese su numero de telefono" value="<?php echo $buscarusuario['telefono']  ?>" name="Telefono" maxlength="8" class="form-control" required <?php echo $disponible;?>>
            <div class="validate"></div>
        </div>
    </div>


    <div class="form-group">
        <input type="hidden" value="<?php echo $color ?>" name="color">
        <input type="hidden" value="<?php echo $detalles ?>" name="Detalles">
        <input type="hidden" value="<?php echo $id ?>" name="id">
        <input type="hidden" value="<?php echo $ordenes ?>" name="cantidadproductos">
        <input type="hidden" value="<?php echo $subtotal ?>" name="subtotal">
        <input type="hidden" value="<?php echo $total ?>" name="total">


    </div>


    <div class="form-group" style="text-align:center">
        <input type="submit" value="Comprar">


    </div>
</form>




<?php
    }
    
}

class metricas_administrativas{
    public $estado;
   public function pendientesdeentrega(){
        $con=new Conexion_DB();
        $estado=$this->estado='Si';
        
        $conteopendiente=$con->conectar()->prepare("Select * from productos_comprados where entregado='No' and is_valido='Si'");
        $conteopendiente->execute();
        $conteo=$conteopendiente->rowcount();
       
          echo "<div class='col-lg-3 col-6 text-center'>
            <span data-toggle='counter-up'>
            $conteo
            </span>";
            echo "<p><a href='estadoproductos.php?Estado=$estado' >Ordenes pendientes de entrega</a></p>
          </div>";
        
    }
    
    public function pendientedevalidar(){
        $con=new Conexion_DB();
         $estado=$this->estado='No';
        $conteopendiente=$con->conectar()->prepare("Select * from productos_comprados where entregado='No' and is_valido='No'");
        $conteopendiente->execute();
        $conteo=$conteopendiente->rowcount();
      
           
        
       echo" <div class='col-lg-3 col-6 text-center'>
            <span data-toggle='counter-up'>$conteo</span>";
         echo "<p><a href='estadoproductos.php?Estado=$estado' >Ordenes sin validar</a></p>
          </div>";
           
    }
     
}

class registros{
    public $nombre;
    public $apellido;
    public $correo;
    public $genero;
    public $pass;
    public $edad;
    public $telefono;
    public $direccion;
    public $rango;
    
    public function registrousuario(){
        
        $conexion= new Conexion_DB();
    
        
        $codigosecreto=$this->nombre.$this->apellido;
        $codigosecreto=str_shuffle($codigosecreto);
        
        $nombre=$this->nombre;
        $apellido=$this->apellido;
        $correo=$this->correo;
        $genero=$this->genero;
        $pass=$this->pass;
        $edad=$this->edad;
        $telefono=$this->telefono;
        $direccion=$this->direccion;
        $mensaje="Gracias por registrarte tu codigo secreto es $codigosecreto, con este codigo puedes recuperar tu contraseña si la llegaras a perder";
        $asunto="Registro en flores";
        
        
        $nuevousuario=$conexion->conectar()->prepare("INSERT INTO usuarios VALUES(NULL,:nombre,:apellido,:correo,:genero,:pass,:edad,:telefono,:direccion,:codigosecreto)");
        
         $nuevousuario->bindParam (":nombre", $nombre);
         $nuevousuario->bindParam (":apellido", $apellido);
         $nuevousuario->bindParam (":correo", $correo);
         $nuevousuario->bindParam (":genero", $genero);
         $nuevousuario->bindParam (":pass", $pass);
         $nuevousuario->bindParam (":edad", $edad);
         $nuevousuario->bindParam (":telefono", $telefono);
         $nuevousuario->bindParam (":direccion", $direccion);
         $nuevousuario->bindParam (":codigosecreto", $codigosecreto);
         $nuevousuario->execute();
         $cantidad=$nuevousuario->rowcount();
        
        if($cantidad>0){
            echo "<script>
            alert('El registro ha sido exitoso');
            </script>";
            
            mail($correo,$asunto,$mensaje);
        }else{
             echo "<script>
            alert('Algo fallo intenta de nuevo');
            </script>";
        }
    }
    
    public function Admiregistrousuario(){
        
        $conexion= new Conexion_DB();
    
        
        $codigosecreto=$this->nombre.$this->apellido;
        $codigosecreto=str_shuffle($codigosecreto);
        
        $nombre=$this->nombre;
        $apellido=$this->apellido;
        $correo=$this->correo;
        $genero=$this->genero;
        $pass=$this->pass;
        $edad=$this->edad;
        $telefono=$this->telefono;
        $direccion=$this->direccion;
        $rango=$this->rango;
        $mensaje="Gracias por registrarte tu codigo secreto es $codigosecreto, con este codigo puedes recuperar tu contraseña si la llegaras a perder";
        $asunto="Registro en flores";
        
        
        $nuevousuario=$conexion->conectar()->prepare("INSERT INTO usuariosadmin VALUES(NULL,:nombre,:apellido,:correo,:genero,:pass,:edad,:telefono,:direccion,:codigosecreto,:rango)");
        
         $nuevousuario->bindParam (":nombre", $nombre);
         $nuevousuario->bindParam (":apellido", $apellido);
         $nuevousuario->bindParam (":correo", $correo);
         $nuevousuario->bindParam (":genero", $genero);
         $nuevousuario->bindParam (":pass", $pass);
         $nuevousuario->bindParam (":edad", $edad);
         $nuevousuario->bindParam (":telefono", $telefono);
         $nuevousuario->bindParam (":direccion", $direccion);
         $nuevousuario->bindParam (":codigosecreto", $codigosecreto);
         $nuevousuario->bindParam (":rango", $rango);
         $nuevousuario->execute();
         $cantidad=$nuevousuario->rowcount();
        
        if($cantidad>0){
            echo "<script>
            alert('El registro ha sido exitoso');
            </script>";
            
            mail($correo,$asunto,$mensaje);
        }else{
             echo "<script>
            alert('Algo fallo intenta de nuevo');
            </script>";
        }
    }
}

class actualizarproducto{
    public $codigo;
    
    /* Actualizar producto */
    
    public $nombre;
    public $precio;
    public $tamanio;
    public $cantidad;
    public $descripcion;
    public $colores;
    public $descuento;
    public $promocion;
    
    public function seleccionarproducto(){
        $con= new Conexion_DB();
        
        $producto=$con->conectar()->prepare("SELECT id_productos,nombre from productos");
        $producto->execute();
        
       ?>


<form action='' method='post'>
    <table class="table table-dark">
        <tr>
            <td>
                <h3 style="text-align:center">Selecciona el producto</h3>
            </td>
        </tr>
        <tr>
            <td>
                <select name="producto" id="" class="custom-select">
                    <option value="0">Selecciona el producto</option>
                    <?php
        
        foreach($producto as $producto){
           echo "
            <option style='text-align:center' value='{$producto[0]}'>{$producto[1]}</option>";
        }
        ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="enviar" name="enviar" class="form-control"></td>

        </tr>
    </table>

</form>


<?php
    }
    
    public function detalleproducto(){
        $codigo=$this->codigo;
        $con= new Conexion_DB();
        
        $producto=$con->Conectar()->prepare("SELECT * from productos as pro, detalle_producto as det where id_producto=id_productos and id_productos=:codigo");
        $producto->bindParam (":codigo", $codigo);
        $producto->execute();
        $conteo=$producto->rowcount();
        
        foreach($producto as $producto){}
        
                
               
               if($producto['have_descuento']=='Si'){
                   $estado='checked';
               }else{
               $estado='';    
               }
            
            if($producto['have_promocion']=='Si'){
                   $estado2='checked';
               }else{
               $estado2='';    
               }
               
             
               
        
        if($conteo>0){
           ?>
<form action="" method="post">
    <br>
    <div class="form-row">
        <div class="col">
            <label for="">Nombre del producto</label>
            <input type="text" class="form-control" placeholder="Nombre" value="<?php echo $producto[1] ?>" name="nombre">
        </div>
        <div class="col"><label for="">Precio del producto</label><input type="number" class="form-control" placeholder="Precio" value="<?php echo $producto[2] ?>" name="precio" min="1"></div>
    </div>

    <br>
    <div class="form-row">
        <div class="col">
            <label for="">tamaño del producto</label>
            <select name="tama" id="" class="form-control">
                <option value="0">Seleccione el tamaño</option>
                <option value="1">Pequeño</option>
                <option value="2">Mediano</option>
                <option value="3">Grande</option>
            </select>
        </div>
        <div class="col">
            <label for="">Cantidad en inventario</label>
            <input type="number" class="form-control" placeholder="Cantidad en inventario" value="<?php echo $producto[4] ?>" name="Cantidad" min="0">
        </div>
    </div>

    <br>
    <div class="form-row">
        <div class="col"><textarea name="Descripcion" id="" cols="10" rows="5" class="form-control" placeholder="descripcion">
                <?php echo $producto[6] ?>
                </textarea></div>
    </div>

    <br>
    <div class="form-row">
        <div class="col">
            <label for="">Colores Disponibles</label>
            <input type="text" class="form-control" placeholder="colores disponibles colocando espacio entre los colores" value="<?php echo $producto[5] ?>" name="colores">
        </div>
        <div class="col">
            <label for="">Fecha de ingreso</label>
            <input type="text" class="form-control" readonly value="<?php echo $producto['fecha_de_ingreso'] ?>">
        </div>
    </div>

    <br>
    <div class="form-row">
        <div class="col"><label for="">Tiene descuento</label>
            <select name="Descuento" id="">
                <?php
               if($producto['have_descuento']=='Si'){
                  echo " <option value='{$producto['have_descuento']} '>{$producto['have_descuento']}</option>";
                   echo "    <option value='No'>No</option>";
               }else{
                 echo " <option value='{$producto['have_descuento']} '>{$producto['have_descuento']}</option>";
                   echo "    <option value='Si'>Si</option>";
               }
               ?>
            </select>
        </div>
        <div class="col">
            <label for="">Tiene promocion</label>
            <select name="Promocion" id="">
                <?php
                if($producto['have_promocion']=='Si'){
                  echo " <option value='{$producto['have_promocion']} '>{$producto['have_promocion']}</option>";
                   echo "    <option value='No'>No</option>";
               }else{
                 echo " <option value='{$producto['have_promocion']} '>{$producto['have_promocion']}</option>";
                   echo "    <option value='Si'>Si</option>";
               }
               ?>
            </select>

        </div>
    </div>
    <br>
    <div class="form-row">
        <div class="col"><input type="hidden" class="form-control" value="<?php echo $producto['id_productos'] ?>" name="codigo"> </div>
        <div class="col"><input type="submit" class="form-control" value="Actualizar" name="Update"></div>
        <div class="col">&nbsp;</div>
    </div>
</form>
<?php 
        }else{
            echo "No hay productos con este codigo";
        }
       
    }
    
    public function actualizaproducto(){
        
        
        $con=new Conexion_DB();
        $codigo=$this->codigo;
        $producto=$con->Conectar()->prepare("SELECT * from productos as pro, detalle_producto as det where id_producto=id_productos and id_productos=:codigo");
        $producto->bindParam (":codigo", $codigo);
        $producto->execute();
        
        foreach($producto as $producto){}
      
        
        
        
        $nombre=empty($this->nombre)?$producto['nombre']:$this->nombre ;
        $precio=empty($this->precio)?$producto['precio'] :$this->precio ;
        $tamanio=empty($this->tamanio)?$producto['tamanio'] :$this->tamanio;
        $cantidad=empty($this->cantidad)?$producto['cantidad_en_stock'] :$this->cantidad;
        $descripcion=empty($this->descripcion)?$producto['descripcion'] :$this->descripcion;
        $colores=empty($this->colores)?$producto['color'] :$this->colores;
        
        
        $descuento=$this->descuento;
        $promocion=$this->promocion;
        
        $update=$con->conectar()->prepare("UPDATE productos set nombre=:nombre, precio=:precio, tamanio=:tamanio, cantidad_en_stock=:cantidad,
        color=:color, descripcion=:descripcion where id_productos=:idproducto");
        $update->bindParam (":nombre", $nombre);
        $update->bindParam (":precio", $precio);
        $update->bindParam (":tamanio", $tamanio);
        $update->bindParam (":cantidad", $cantidad);
        $update->bindParam (":color", $colores);
        $update->bindParam (":descripcion", $descripcion);
        $update->bindParam (":idproducto", $codigo);
        $update->execute();
        
        $conteo=$update->rowcount();
        
         $detalle=$con->conectar()->prepare("UPDATE detalle_producto set have_descuento=:tienedescuento, have_promocion=:tienepromocion where id_producto=:idproducto");
        $detalle->bindParam (":tienedescuento", $descuento);
        $detalle->bindParam (":tienepromocion", $promocion);
        $detalle->bindParam (":idproducto", $codigo);
        $detalle->execute();
        
        $conteo2=$detalle->rowcount();
            
           
        
        if($conteo>0 && $conteo2>0){
             echo "<script>alert('Gracias por actualizar el producto')</script>";
        
        }else{
            echo "<script>alert('Fallo al intentar actualizar intenta de nuevo')</script>";
        }
       
        
        
        
        

        
        
    }
}

class persona{
    public $usuario;
    public $pass;
    public $rango;
    /*recuperar*/
    public $email;
    public $codigo;
    
    
    
    public function ingreso(){
        $rango=$this->rango;
        $pass=$this->pass;
        $usuario=$this->usuario;
        
        
        $con= new Conexion_DB();
        
        switch ($rango){
            case 1:
                $busqueda=$con->conectar()->prepare("SELECT * from usuariosadmin where correo=:correo and pass=:pass");
                $busqueda->bindParam (":correo", $usuario);
                $busqueda->bindParam (":pass", $pass);
                //$busqueda->bindParam (":codigosecreto", $codigo);
                $busqueda->execute();
                $conteo=$busqueda->rowcount();
                
                if($conteo>0){
                     
                    echo "<script>alert('Logeo exitoso');
                    window.location.href = 'index.php';
                    </script>";
                    foreach($busqueda as $datos){}
                    $_SESSION['usuarioadmini']=$datos['id_usuario'];
                 }else{
                        echo "<script>alert('Algo fallo')
                        window.location.href = 'interno.php';
                        </script>";
                
                }
                
                break;
            default:
                $busqueda=$con->conectar()->prepare("SELECT * from usuarios where correo=:correo and pass=:pass");
                $busqueda->bindParam (":correo", $usuario);
                $busqueda->bindParam (":pass", $pass);
                $busqueda->execute();
                $conteo=$busqueda->rowcount();
                
               
                if($conteo>0){
                    
                    /* window.location.href = 'index.php'; */
                    echo "<script>alert('Logeo exitoso')
                    window.location.href = 'index.php';
                    </script>";
                    
                    foreach($busqueda as $datos){}
                    
                    
                   
                    $_SESSION['usuario']=$datos['id_usuario'];
                        
                    
                    
                    
                   
                    
                }else{
                        echo "<script>alert('Intenta de nuevo')
                         window.location.href = 'login.php';
                        </script>";
                
                }
                
                break;
        }
    }
    
    public function recuperarcontra(){
        $correo=$this->email;
        $codigo=$this->codigo;
       
        
        $con= new Conexion_DB();
        
         $recuperar=$con->conectar()->prepare("SELECT pass from usuarios where correo=:correo and codigosecreto=:codigo");
         $recuperar->bindParam (":correo", $correo);
         $recuperar->bindParam (":codigo", $codigo);
         $recuperar->execute();
         $conteo=$recuperar->rowcount();
        if($conteo>0){
            foreach($recuperar as $recuperar){}
             $asunto="Recuperacion de password";
            $mensaje="Tu contraseña es:"." ".$recuperar[0]." "."procura recordarla";
                mail($correo,$asunto,$mensaje);
            
            echo "<script>alert('La contraseña ha sido enviado a tu correo')</script>"; 
             echo "<script> window.location.href = 'login.php' </script>";
        }else{
            echo "<script>alert('te has equivodado intenta de nuevo')</script>"; 
            echo "<script> window.location.href = 'login.php?olvido=true' </script>";
        }
    }
    
    public function datosusuario(){
        $codigo=$this->codigo;
        
        $con= new Conexion_DB();
        
        $buscardatos= $con->conectar()->prepare("SELECT *
                                                 FROM usuarios where 
                                                 id_usuario=:codigo ");
        $buscardatos->bindParam(':codigo',$codigo);
        $buscardatos->execute();
        
        foreach($buscardatos as $usuario){
            
        }
       return $usuario[1];
    }
    
      function logeoexitoso(){
        if(isset($_SESSION['usuario'])){
           echo "<script>
           window.location.href='index.php';
           </script>";
       } 
    }
}

class funcionesadmin{
    
    function buscar_usuario(){
        $con= new Conexion_DB();
    $codigo=$_SESSION['usuarioadmini'];
    $buscar=$con->conectar()->prepare('SELECT nombre
    FROM usuariosAdmin
    WHERE id_usuario=:idUser');
    $buscar->bindParam(':idUser',$codigo);
    $buscar->execute();
    
    if($buscar->rowcount()){
        foreach($buscar as $buscar){
            $nombre=$buscar['0'];
        }
    }
    
    return $nombre;
    }
    
    function permiso(){
       if(!isset($_SESSION['usuarioadmini'])){
           echo "<script>
           window.location.href='index.php';
           </script>";
       } 
    }
    
    function logeoexitoso(){
        if(isset($_SESSION['usuarioadmini'])){
           echo "<script>
           window.location.href='index.php';
           </script>";
       } 
    }
    
}

class comentarios{
    public $comentarios;
    public $usuarios;
    
    function registrarcomentario(){
            $comentario=$this->comentarios;
            $usuario=$this->usuarios;
            date_default_timezone_set('America/guatemala');
            $fecha=date('Y-m-d-H-i');
        
        
        $con= new Conexion_DB();
        
        $insertar=$con->conectar()->prepare("INSERT INTO comentarios VALUES(NULL,'$comentario','$usuario',' ','$fecha',' ',' ')");
        $insertar->execute();
        
        $conteo=$insertar->rowcount();
        
        if($conteo>0){
            echo "<script>alert('Comentario ingresado con exito')</script>";
        echo $usuario.$comentario;
        }
    }
}
?>
