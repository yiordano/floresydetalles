<?php
include("main.php");

$menu= new sitio_web();

$componente= new variadas();

$persona=new persona();
?>







<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>KnightOne Bootstrap Template - Index</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <?php
    $menu->encabezado();
?>
<?php
    if(isset($_POST['comentarios'])){
        $comentarios= new comentarios();
        $comentarios->comentarios=$_POST['comentarios'];
        $comentarios->usuarios=$_POST['nombre'];
        $comentarios->registrarcomentario();
    }
    ?>
    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex flex-column justify-content-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <h1>KnightOne - Create Bootstrap Website Template</h1>
                    <h2>We are team of designers making websites with Bootstrap</h2>
                    <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
                </div>
            </div>
        </div>
    </section><!-- End Hero -->

    <main id="main">

        <!-- ======= About Us Section ======= -->

        <!-- ======= Services Section ======= -->

        <!-- ======= Cta Section ======= -->

        <!-- ======= Features Section ======= -->

        <!-- ======= Clients Section ======= -->

        <!-- ======= Counts Section ======= -->

        <!-- ======= Portfolio Section ======= -->


        <!-- ======= Faq Section ======= -->


        <!-- ======= Contact Section ======= -->
        <section id="registrar" class="registrar">

            <div class="container">

                <div class="row mt-5">

                    <div class="col-lg-8 mt-5 mt-lg-0">

                        <style>
                            #comentario {
                               
                                    background: #009961;
                                    border: 0;
                                    padding: 10px 30px;
                                    color: #fff;
                                    transition: 0.4s;
                                    border-radius: 50px;
                                }
                                
                                #comentario:hover{
                                    background: #00b371;
                                }

                        </style>

                        <h3 style="text-align:center">Ingresa tus comentarios</h3>

                        <!--                        <form method="post">-->
                        <div class="form-row">
                            <div class="col">
                                <label for="Email">Ingresa tu comentario</label>
                                <textarea name="comentarios" rows="2" class="form-control" required maxlength="150"  id="comentarios" >
                                    </textarea>

                            </div>
                        </div>
                        <br>

                        <?php 
                                           
                                           if(isset( $_SESSION['usuario'])){
                                             $persona->codigo=$_SESSION['usuario'];
                                             $valido='readonly';
                                             $nombre= $persona->datosusuario();
                                           }else{
                                               $nombre='';
                                               $valido='';
                                           }
                                           
                                           ?>

                        <div class="form-row">
                            <div class="col">
                                <label for="">Ingresa tu nombre</label>
                                <input type="text" id="nombre" class="form-control" required value="<?php
                                             echo $nombre;
                                            ?>" <?php echo $valido; ?>>
                            </div>
                        </div>

                        <br>


                        <div class="form-row">
                            <div class="col">
                                &nbsp;
                            </div>
                            <div class="col">
                                <input type="button" class="form-control" name="comentario" value="comentario" id='comentario' required>
                            </div>
                            <div class="col">
                                &nbsp;
                            </div>
                        </div>
                        <br>


                        <div id="respuesta"></div>
                    </div>

                </div>

            </div>
        </section><!-- End Contact Section -->

      



    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="container">
            <h3>KnightOne</h3>
            <p>Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi fuga maxime saepe commodi placeat.</p>
            <div class="social-links">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
            <div class="copyright">
                &copy; Copyright <strong><span>KnightOne</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/ -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </div>
    </footer><!-- End Footer -->

    <div id="preloader"></div>
    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
    <script src="assets/vendor/counterup/counterup.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/venobox/venobox.min.js"></script>
    <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

<script>
    $('#comentario').on('click',function(){
      var parametros = {
                                    "comentarios": $("#comentarios").val(),
                                    "nombre":   $("#nombre").val()
                                };
                                $.ajax({
                                    data: parametros,
                                    url: 'comentarios.php',
                                    type: 'post',
                                    beforeSend: function() {
                                        $("#respuesta").html("Procesando, espere por favor...");
                                    },
                                    success: function(response) {
                                        let mensaje="gracias por esperar tu comentario ha sido procesado con exito";
                                       $("#respuesta").html("<p><strong>" + mensaje + "</strong></p>");
                                       alert('Comentario ingresado con exito');
                                        window.location.href='index.php#faq';
                                    }
                                });
    });
    </script>
</body>

</html>
