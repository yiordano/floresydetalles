<?php

include("../main.php");
$conecta=new Conexion_DB();
$menu= new sitio_web();

 $mes=empty($_POST["Mesabuscar"])?0:$_POST["Mesabuscar"];
$anio=empty($_POST["anioabuscar"])?0:$_POST["anioabuscar"];



        $producto= new Conexion_DB();
        $Productosamostrar =$producto->conectar()->prepare("SELECT * FROM productos as p, detalle_producto as dp where dp.mes=:mes and dp.anio=:anio and  p.id_productos=dp.id_detalle_producto ");
                $Productosamostrar->bindParam (":mes", $mes);
                $Productosamostrar->bindParam (":anio", $anio);
                $Productosamostrar->execute();

$conteo=$Productosamostrar->rowcount();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>KnightOne Bootstrap Template - Index</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="../assets/img/favicon.png" rel="icon">
  <link href="../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="../assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="../assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="../assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <?php
    $menu->encabezado_interno();
    ?>

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xl-8">
          <h1>KnightOne - Create Bootstrap Website Template</h1>
          <h2>We are team of designers making websites with Bootstrap</h2>
          <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
        </div>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

   

<?php
  
      if($conteo>0){
          
          ?>
          
          <section id="registrar" class="registrar">
    <div class="container">

        <div class="section-title">
            <h2>Catalogo</h2>
        </div>
    </div>

   

    <div class="container">

<div class="row mt-6">
            <div class="col-lg-8 mt-5 mt-lg-0">

                    
                    
    
<section id="portfolio" class="portfolio">
    <div class="container">

        <div class="section-title">
            <h2>Productos</h2>

        </div>

        <div class="row">
            <div class="col-lg-12 d-flex justify-content-center">
                <ul id="portfolio-flters">
                    <li data-filter="*" class="filter-active">All</li>
                    <li data-filter=".filter-app">Pequeño</li>
                    <li data-filter=".filter-card">Mediano</li>
                    <li data-filter=".filter-web">Grande</li>
                </ul>
            </div>
        </div>

        <div class="row portfolio-container">

            <?php
       
    
         
       
           foreach($Productosamostrar as $Productosamostrar){
               $nombre=$Productosamostrar["nombre"];
               
               $precio=$Productosamostrar["precio"];
               $id=$Productosamostrar["id_productos"];
                $cantidad=$Productosamostrar["cantidad_en_stock"];
               $imagen=$Productosamostrar["imagen2"];
               $tipo=$Productosamostrar["tipoimage1"];
               
               switch($Productosamostrar["tamanio"]){
                  
                   case 1:
                 echo "
                        <div class='col-lg-4 col-md-6 portfolio-item filter-app'>";
                        echo "<img src='data:$tipo;base64," . base64_encode($imagen)."' class='img-fluid' >";
             echo"   <div class='portfolio-info'>
                    <h4>$nombre</h4>
                    <p>Q $precio</p>
                    <p>Cantidad en stock: $cantidad </p>
                  <a href='data:$tipo;base64," . base64_encode($imagen)."' data-gall='portfolioGallery' class='venobox preview-link' title='$nombre'>  <i class='bx bx-plus'></i></a>
                    <a href='../portfolio-details.php?Id=$id' class='details-link' title='mas detalles'><i class='bx bx-link'></i></a>
                </div>
            </div>";
                       break;
                       
                   case 2:
                       echo "
                        <div class='col-lg-4 col-md-6 portfolio-item filter-card'>";
                        echo "<img src='data:$tipo;base64," . base64_encode($imagen)."' class='img-fluid'>";
             echo"   <div class='portfolio-info'>
                    <h4>$nombre</h4>
                    <p>Q $precio</p>
                     <p>Cantidad en stock: $cantidad </p>
                    <a href='data:$tipo;base64," . base64_encode($imagen)."' data-gall='portfolioGallery' class='venobox preview-link' title='$nombre'>
                    <i class='bx bx-plus'></i></a>
                    <a href='../portfolio-details.php?Id=$id' class='details-link' title='mas detalles'><i class='bx bx-link'></i></a>
                </div>
            </div>";
                       
                       break;
                       
                   case 3:
                       
                  echo "
                        <div class='col-lg-4 col-md-6 portfolio-item filter-web'>";
                        echo "<img src='data:$tipo;base64," . base64_encode($imagen)."' class='img-fluid'>";
             echo"   <div class='portfolio-info'>
                    <h4>$nombre</h4>
                    <p>Q $precio</p>
                     <p>Cantidad en stock: $cantidad </p>
                   <a href='data:$tipo;base64," . base64_encode($imagen)."' data-gall='portfolioGallery' class='venobox preview-link' title='$nombre'> <i class='bx bx-plus'></i></a>
                    <a href='../portfolio-details.php?Id=$id' class='details-link' title='mas detalles'><i class='bx bx-link'></i></a>
                </div>
            </div>";
                      
                       break;
               }
           }

            ?>



        </div>

    </div>
</section><!-- End Portfolio Section -->

 
    
                    

                    </div>
                    </div>
    </div>
     
              
               




      
<!--        </div>-->


</section><!-- End Contact Section -->


          
      <?php
      }else{
          echo "<script>alert('No hay resultados para tu busqueda prueba otra vez')
          
          window.location.href = '../catalogo.php';
          </script>";
      }
      
  
 ?>


  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <h3>KnightOne</h3>
      <p>Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi fuga maxime saepe commodi placeat.</p>
      <div class="social-links">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
      <div class="copyright">
        &copy; Copyright <strong><span>KnightOne</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

  <!-- Vendor JS Files -->
  <script src="../assets/vendor/jquery/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="../assets/vendor/php-email-form/validate.js"></script>
  <script src="../assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="../assets/vendor/counterup/counterup.min.js"></script>
  <script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="../assets/vendor/venobox/venobox.min.js"></script>
  <script src="../assets/vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="../assets/js/main.js"></script>

</body>

</html>


