<?php

include("main.php");
$conecta=new Conexion_DB();
$menu= new sitio_web();
$conteo=new conteo();
$componentes=new variadas();

$is_valido=new funcionesadmin();

$is_valido->permiso();

$stm=$conecta->conectar()->prepare("SELECT * FROM productos as p,
detalle_producto as dp where p.id_productos=dp.id_producto");

$stm->execute();



if ( !empty($_POST["como"]) && is_array($_POST["como"]) ) { 
     $codigovalor=$_POST["como"];
    
     foreach($codigovalor as $separado){
         
           $lista=explode(".",$separado);
        
         $var1= $lista[0];
         $var2= $lista[1];   
         
         $stm=$conecta->conectar()->query("Update detalle_producto set darbaja='$var1' where id_producto=$var2");
         
         header("Location: adm_productos.php");
     }
 
}

   

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>KnightOne Bootstrap Template - Index</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <?php
    $menu->encabezado();
    ?>

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xl-8">
          <h1>KnightOne - Create Bootstrap Website Template</h1>
          <h2>We are team of designers making websites with Bootstrap</h2>
          <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
        </div>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

   


    <!-- ======= Contact Section ======= -->
  <section id="registrar" class="registrar">
    <div class="container">

        <div class="section-title">
            <h2>Estado de los productos <producto></producto></h2>
        </div>
    </div>

    <div>

        <!-- 
        <iframe style="border:0; width: 100%; height: 350px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" allowfullscreen></iframe>
      -->

    </div>

    <div class="container">


        <div class="row mt-5">

            <div class="col-lg-8 mt-5 mt-lg-0">

   <form action="" method="post">
    
<table class="table table-dark">
    <tr>
    <td>Nombre</td>
    <td>Precio</td>
    <td>cantidad en inventario</td>
    <td>Estado</td>
    <td>dar de baja o alta</td>
    </tr>
    <?php
   foreach($stm as $result)
    {
         $nombre=$result["nombre"];
         $precio=$result["precio"];
         $cantidad=$result["cantidad_en_stock"];
         $darbaja=$result["darbaja"];
         $identificador=$result["id_producto"]; 
        
         $conteo->estado=$darbaja;
         $baja=$conteo->estadoproducto();
         $componentes->estado=$darbaja;
         $componentes->id=$identificador;
         $input=$componentes->dibujarcomponente();
       
        
           
     echo "<tr>
     
     <td>$nombre</td> 
     <td>$precio</td> 
     <td>$cantidad</td> 
     <td>$baja</td>
     <td>$input</td>
     
     
     
     </tr>";
        
        
                

    }
    ?>
  
 </table>
        <input type="submit" value="Actualizar">

   </form>

               




            </div>

        </div>

    </div>
</section><!-- End Contact Section -->





  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <h3>KnightOne</h3>
      <p>Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi fuga maxime saepe commodi placeat.</p>
      <div class="social-links">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
      <div class="copyright">
        &copy; Copyright <strong><span>KnightOne</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>


