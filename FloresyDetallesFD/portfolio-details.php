<?php

include("main.php");

$conexion=new Conexion_DB();
$utilidad=new sitio_web();
$varias=new variadas();

?>






<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Portfolio Details - KnightOne Bootstrap Template</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->

<?php
    $utilidad->encabezado();
    $codigo=$_GET['Id'];
    $conexion->conectar();
    
    $conectar=$conexion->conectar()->prepare("SELECT * FROM productos as productos, detalle_producto as detalle_producto where productos.id_productos=:id_detalle and
detalle_producto.id_detalle_producto=:id_detalle_producto");
    
    

   
     $conectar->bindParam (":id_detalle", $codigo);
    $conectar->bindParam (":id_detalle_producto", $codigo);
     $conectar->execute();
    
    foreach($conectar as $conectar){
     $imagen=$conectar["imagen1"];
     $imagen1=$conectar["imagen2"];
     $imagen2=$conectar["imagen3"];
     $tipo1=$conectar["tipoimage1"];
     $tipo2=$conectar["tipoimage2"];
     $tipo3=$conectar["tipoimage3"];
    }
    
    
?>
 
<?php
            echo "<script> let precio=$conectar[precio]</script>";
            echo "<script> let descuento='$conectar[have_descuento]'</script>";
            echo "<script> let identificador='$codigo'</script>";
            echo "<script> let cantidadadescontar='$conectar[cantidadendescuento]'</script>";
?>
 
 <style>
     .Detalles {
         display: none;
         transition: all 3.5s;
     }
     
   
    </style>
  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Inicio</a></li>
          <li>Detalles del producto</li>
        </ol>
        <h2>Detalle del producto</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="portfolio-details-container">

          <div class="owl-carousel portfolio-details-carousel">
         <?php
               echo "<img src='data:$tipo1;base64," . base64_encode($imagen)."'>";
             echo "<img src='data:$tipo2;base64," . base64_encode( $imagen1)."'>";
              echo "<img src='data:$tipo3;base64," . base64_encode($imagen2)."'>";
           ?>   
              </div>

          <div class="portfolio-info">
            <h3><?php  echo $conectar["nombre"]; ?>
</h3>
           <form action="Compra.php" method="post">
            <ul>
              <li><strong>Cantidad en inventario</strong>:  <?php echo $conectar["cantidad_en_stock"]   ?></li>
              <li><strong>precio por unidad</strong>: <?php echo $conectar["precio"]   ?></li>
              <li><strong>tamaño</strong>:  <?php echo $varias->tamanio($conectar["tamanio"])   ?></li>
               <?php
                if($conectar["have_descuento"]=='Si'){
                    ?>
                    
                    <li><strong>Descuento: </strong><div style="color:skyblue"><?php echo "<b>".$conectar["cantidadendescuento"]."%"."</b>"   ?></div></li>
                <?php
                }
                 ?>
              
               <?php
                if($conectar["have_promocion"]=='Si'){
                    ?>
                    
                    <li><strong>Promocion: </strong><div style="color:red"><?php echo "<b>".$conectar["infopromocion"]."</b>"   ?></div></li>
                <?php
                }
                 ?>
                 <br>
               <li><strong>Fecha de ingreso</strong>: <?php echo $conectar["fecha_de_ingreso"]   ?></li>
               <li><strong>Cuantos productos necesita: </strong><input type="number"  id="Unidadacomprar" style="text-align:center"  min=1 
               max=<?php echo $conectar["cantidad_en_stock"]   ?>  >  </li>
               <li><strong>Otros detalles: </strong><input type="checkbox" 
               id='Dedicatoria'></li>
               <li> <b> Colores disponibles: </b> <?php $varias->ListadoColores=$conectar["color"];
                   $varias->colores();
                   ?>
               
                   </li>
                   
                   <li Class='Detalles' id='Detalles'><textarea name="Detalles" id="" cols="15" rows="10" placeholder="Puedes ingresar todos los detalles que desees" class="form-control" style="color:black">
                       
                   </textarea></li>
               </ul>
           <ul style="border-top: 1px solid #eee;">
              <li>&nbsp;</li>
               <li class="subtotal"><strong>subtotal: </strong> <div></div> </li>
                <li class="total"><strong>total: </strong> <div></div></li>
                   <li><input class="codigogeneral" type="hidden" name="codigogeneral"></li>
                <li><input type="submit" value="Comprar"></li>
           </ul>
           </form>
          </div>

        </div>

        <script src="assets/vendor/jquery/jquery.min.js"></script>
      
       <script>
           
           $('#Dedicatoria').click(function(){
           
         if($("#Dedicatoria").is(':checked')) {  
          
          $(".Detalles").removeClass('Detalles');
        } else {  
            $("#Detalles").addClass('Detalles');
        } 
               
           });
          
          $( "#Unidadacomprar" ).change(function() {
               let max = parseInt($(this).attr('max'));
              if(max<$( "#Unidadacomprar" ).val()){
                     $( "#Unidadacomprar " ).text( max );
              }
              
             let unidad=$( "#Unidadacomprar" ).val()*precio;
             let unidadoriginal=unidad;
             $( ".subtotal div" ).text("Q "+unidad );
              
             if(descuento=="Si"){
                 let descuento=(unidad*cantidadadescontar)/100;
                 unidad=unidad-descuento;
                unidad=parseInt(unidad,10);
                 
                 
             }
               $( ".total div" ).text("Q "+unidad );
              
              $( ".precio" ).attr("value",unidad );
              $( ".precioriginal" ).attr("value",unidadoriginal );
               $( ".identificador" ).attr("value",identificador );
               $( ".codigogeneral" ).attr("value",identificador+" " + unidadoriginal+" " + unidad+" " + $( "#Unidadacomprar" ).val());
               
            });
           
          </script>
       
        <div class="portfolio-description">
          <h2>Descripcion de: <?php  echo "<b>".$conectar["nombre"]."</b>"; ?> </h2>
          <p>
            <?php echo $conectar["descripcion"]   ?>
          </p>
        </div>

      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <h3>KnightOne</h3>
      <p>Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi fuga maxime saepe commodi placeat.</p>
      <div class="social-links">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
      <div class="copyright">
        &copy; Copyright <strong><span>KnightOne</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>