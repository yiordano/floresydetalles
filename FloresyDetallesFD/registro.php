<?php
include("main.php");

$menu= new sitio_web();

$componente= new variadas();


$persona=new persona();


$persona->logeoexitoso();

?>

<?php

if(isset($_POST['Enviar'])){
   $registro= new registros();
 
    
$nombre=empty($_POST["nombre"])?'CF':$_POST["nombre"];
$apellido=empty($_POST["apellido"])?'CF':$_POST["apellido"];
$Correo=empty($_POST["email"])?'prueba@gmail.com':$_POST["email"];
$genero=empty($_POST["Genero"])?'0':$_POST["Genero"];
$pass=empty($_POST["password"])?'root':$_POST["password"];
$edad=empty($_POST["edad"])?18:$_POST["edad"];
$telefono=empty($_POST["telefono"])?54674345:$_POST["telefono"];
$direccion=empty($_POST["Direccion"])?'sin direccion':$_POST["Direccion"];
  
    $registro->nombre=$nombre;
    $registro->apellido=$apellido;
    $registro->correo=$Correo;
    $registro->genero=$genero;
    $registro->pass=$pass;
    $registro->edad=$edad;
    $registro->telefono=$telefono;
    $registro->direccion=$direccion;
    
    
    $registro->registrousuario();
}

?>






<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>KnightOne Bootstrap Template - Index</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
  * Template Name: KnightOne - v2.1.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <?php
    $menu->encabezado();
    ?>

    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex flex-column justify-content-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <h1>KnightOne - Create Bootstrap Website Template</h1>
                    <h2>We are team of designers making websites with Bootstrap</h2>
                    <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
                </div>
            </div>
        </div>
    </section><!-- End Hero -->

    <main id="main">

        <!-- ======= About Us Section ======= -->

        <!-- ======= Services Section ======= -->

        <!-- ======= Cta Section ======= -->

        <!-- ======= Features Section ======= -->

        <!-- ======= Clients Section ======= -->

        <!-- ======= Counts Section ======= -->

        <!-- ======= Portfolio Section ======= -->


        <!-- ======= Faq Section ======= -->


        <!-- ======= Contact Section ======= -->
        <section id="registrar" class="registrar">
            <div class="container">

                <div class="section-title">
                    <h2>Registro de usuario</h2>
                </div>
            </div>

            <div>

                <!-- 
        <iframe style="border:0; width: 100%; height: 350px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" allowfullscreen></iframe>
      -->

            </div>
            <div class="container">


                <div class="row mt-5">

                    <div class="col-lg-8 mt-5 mt-lg-0">

                            <h3 style="text-align:center">Ingresa tus datos</h3>
                            <br>
                        <form method="post">
                            <div class="form-row">
                                <div class="col">
                                   <label for="nombre">Tu nombre</label>
                                    <input type="text" class="form-control" placeholder="Nombre" id="nombre" name="nombre" required>
                                </div>
                                <div class="col">
                                   <label for="Apellido">Tu apellido</label>
                                    <input type="text" class="form-control" placeholder="Apellido" id="apellido" name="apellido" required>
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col">
                                <label for="email">Correo electronico</label>
                                <input type="email" class="form-control" placeholder="Correo Electronico" id="email" name="email" required>
                                </div>
                                <div class="col">
                                 <label for="Genero">Genero</label>
                                <select name="Genero" id="Genero" class="form-control form-control-lg" id="Genero" required >
                                    <option value="0">Genero</option>
                                    <option value="1">Masculino</option>
                                    <option value="2">Femenino</option>
                                </select>
                                
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col">
                                  <label for="password">Contraseña</label>
                                   <input type="password" class="form-control" id="password" placeholder="Contraseña"  pattern=".{6,}" name="password" required maxlength="8" >
                                </div>
                                <div class="col">
                                    <label for="repass">Repite la contraseña </label>
                               
                                    <input type="password" class="form-control" id="repass" placeholder="Repite la contraseña"  pattern=".{6,}" required maxlength="8" >
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col">
                                 <label for="edad">Edad </label>
                                 <?php $componente->edad(); ?>
                                </div>
                                
                               <div class="col">
                                    <label for="telefono">Telefono </label>
                                    <input type="number" class="form-control" name="telefono" placeholder="Numero telefonico" maxlength="8"  id="telefono" required>
                                </div>
                            </div>
                            <br>
                             <div class="form-row">
                                  <div class="col">
                                 
                                  <label for="Direccion">Direccion </label>
                                    
                                       <input type="text" class="form-control" name="Direccion" placeholder="Direccion" id="Direccion" required>
                                </div>
                              
                            </div>
                            <br>
                            <div class="form-row">
                                 <div class="col">
                                   &nbsp;
                                </div>
                                  <div class="col">
                                    <input type="submit" class="form-control" name="Enviar" value="Registrar" required>
                                </div>
                               <div class="col">
                                   &nbsp;
                                </div>
                            </div>
                        </form>




                    </div>

                </div>

            </div>
        </section><!-- End Contact Section -->

    <script src="assets/vendor/jquery/jquery.min.js"></script>
            
               <script>
                $("#repass" ).change(function() {
               let pass=$("#password" ).val();
               let repass=$("#repass" ).val();
               
                    if(pass==repass){
                    $("#password" ).attr('type','password');
                      $("#repass" ).attr('type','password');
                        
                    }else{
                        alert('Las contraseñas son diferentes');
                      $("#password" ).attr('type','text');
                      $("#repass" ).attr('type','text');
                       }
                    
                        });
                </script>




    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="container">
            <h3>KnightOne</h3>
            <p>Et aut eum quis fuga eos sunt ipsa nihil. Labore corporis magni eligendi fuga maxime saepe commodi placeat.</p>
            <div class="social-links">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
            <div class="copyright">
                &copy; Copyright <strong><span>KnightOne</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/ -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </div>
    </footer><!-- End Footer -->

    <div id="preloader"></div>
    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
    <script src="assets/vendor/counterup/counterup.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/venobox/venobox.min.js"></script>
    <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>

</html>
